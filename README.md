# Deprecated MoSBi repository

This repository is deprecated. Please download and follow the latest updates on the github repository: [https://github.com/tdrose/mosbi](https://github.com/tdrose/mosbi)

Or alternatively download mosbi from bioconductor: [https://bioconductor.org/packages/mosbi/](https://bioconductor.org/packages/mosbi/)

If you use the software in a publication cite: 
> Rose et al. “MoSBi: Automated signature mining for molecular stratification and subtyping.” _Proceedings of the National Academy of Sciences_ (2022), 119(16), e2118210119. [https://doi.org/10.1073/pnas.2118210119](https://doi.org/10.1073/pnas.2118210119). 
